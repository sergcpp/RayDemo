[![pipeline status](https://gitlab.com/sergcpp/raydemo/badges/master/pipeline.svg)](https://gitlab.com/sergcpp/raydemo/commits/master)

Test application for a Ray library bundled with a lot of heavy test scenes. Mostly needed for CI checks.
  - Library part can be found here: https://github.com/sergcpp/Ray
  - Links to the original test scenes:  \
    https://benedikt-bitterli.me/resources/  \
    https://www.blender.org/download/demo-files/  \
    https://www.intel.com/content/www/us/en/developer/topic-technology/graphics-research/samples.html  \
    https://developer.nvidia.com/orca/amazon-lumberyard-bistro \
    https://wirewheelsclub.com/models/1965-ford-mustang-fastback \
    https://evermotion.org/shop/show_product/scene-1-ai43-archinteriors-for-blender/14564 \
    https://www.blendermarket.com/products/blender-eevee-modern-villa
